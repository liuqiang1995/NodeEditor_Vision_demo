﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static 视觉框架测试.Form2;
using 视觉框架测试.Tool;
using BaslerSDK;
using HalconDotNet;

namespace 视觉框架测试
{
    public partial class Basler : Form
    {
        Tool_Basler Tool_Basler = new Tool_Basler();
        static Basler basler_frm;
        public static event Action<Tool_Basler> BaslerEvenHandler;
        public Basler()
        {
            InitializeComponent();
        }
        public Basler(Tool_Basler tool_basler)
        {
            InitializeComponent();
            Tool_Basler = tool_basler;
        }

        public static Basler SingleShow(Tool_Basler tool_Basler)
        {
            if (basler_frm == null)
            {
                basler_frm = new Basler(tool_Basler);
            }
            return basler_frm;
        }
        private void Basler_Load(object sender, EventArgs e)
        {
           
            foreach (string item in Tool_Basler.Cam_state.Keys)
            {
                comboBox1.Items.Add(item + "是否打开:" + Tool_Basler.Cam_state[item].ToString());
            }
        }

        private void Basler_FormClosing(object sender, FormClosingEventArgs e)
        {
            basler_frm = null;
        }

        private void Basler_Shown(object sender, EventArgs e)
        {
            DisplayVal(Tool_Basler);
        }

        private void DisplayVal(Tool_Basler tool_Basler)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tool_Basler.myBasler.SetSoftwareTrigger();
            Tool_Basler.myBasler.SendSoftwareExecute();
            Tool_Basler.CallbackImage += Tool_Basler_CallbackImage; ;
        }

        private void Tool_Basler_CallbackImage(HalconDotNet.HObject image)
        {
            HOperatorSet.GetImageSize(image, out HTuple width, out HTuple height);
            HOperatorSet.SetPart(hWindowControl1.HalconWindow, 0, 0, height, width);
            hWindowControl1.HalconWindow.DispObj(image);
            Tool_Basler.Image?.Dispose();
            Tool_Basler.Image = image;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var sn = comboBox1.Text.Split('是');
            Tool_Basler.Ini_Cam(sn[0]);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.SetVal(ref Tool_Basler);
            BaslerEvenHandler?.Invoke(Tool_Basler);


        }

        private void SetVal(ref Tool_Basler tool_Basler)
        {
            
        }
    }
}
