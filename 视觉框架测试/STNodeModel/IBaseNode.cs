﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 视觉框架测试.Tool;

namespace 视觉框架测试.STNodeModel
{
    public interface IBaseNode
    {
        /// <summary>
        /// 显示Tool设置窗口
        /// </summary>
        void Show_Set_Form();
        /// <summary>
        /// 用于获取Node对应的Tool类型
        /// </summary>
        /// <returns></returns>
        BaseTool Get_Tool();
        /// <summary>
        /// 用于设置Node对应的Tool
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        void Set_Tool<T>(T t);
    }
}