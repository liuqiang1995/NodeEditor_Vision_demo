﻿using HalconDotNet;
using ST.Library.UI.NodeEditor;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 视觉框架测试.Tool;
using static 视觉框架测试.Basler;
using static 视觉框架测试.Form3;

namespace 视觉框架测试.STNodeModel
{
    [STNode("图像采集", "LQ", "2023/1/9", "V1.0", "Image Node")]
    public class STN_Basler : STNode, IBaseNode
    {
        Tool_Basler Tool_Basler = new Tool_Basler();
        STNodeOption  m_op_img_out;
        protected override void OnCreate()
        {
            base.OnCreate();
            this.Title = "Basler相机";
            HandledSetimg handledimg = new HandledSetimg(Delegate_img);
            Form3.handledSetimg = handledimg;
            m_op_img_out = this.OutputOptions.Add("输出", typeof(BaseTool), true);
            m_op_img_out.Connecting += M_op_img_out_Connecting;
            m_op_img_out.DisConnected += M_op_img_out_DisConnected;
            Tool_Basler.IsFist = true;
            Tool_Basler.STN_guid = this.Guid;
        }
        protected override void OnOwnerChanged()
        {  //向编辑器提交数据类型颜色
            base.OnOwnerChanged();
            if (this.Owner == null) return;
            this.Owner.SetTypeColor(typeof(BaseTool), Color.DarkCyan);
        }
        private void M_op_img_out_DisConnected(object sender, STNodeOptionEventArgs e)
        {
            Tool_Basler.OutputNode_guid.Remove(e.TargetOption.Owner.Guid);
        }

        private void M_op_img_out_Connecting(object sender, STNodeOptionEventArgs e)
        {
            Tool_Basler.OutputNode_guid.Add(e.TargetOption.Owner.Guid);
        }

        private void Delegate_img(HObject himage)
        {
            if (himage.IsInitialized())
            {
                m_op_img_out.TransferData(new BaseTool { Image = himage }, true);

            }
        }
        public BaseTool Get_Tool()
        {
            return Tool_Basler;
        }

        public void Set_Tool<T>(T t)
        {
            
        }

        public void Show_Set_Form()
        {
            Basler.SingleShow(Tool_Basler).Show();
            Basler.BaslerEvenHandler += Basler_BaslerEvenHandler;
        }

        private void Basler_BaslerEvenHandler(Tool_Basler tool_Basler)
        {
            Tool_Basler = tool_Basler;
            m_op_img_out.TransferData(tool_Basler);
        }

    }
}
