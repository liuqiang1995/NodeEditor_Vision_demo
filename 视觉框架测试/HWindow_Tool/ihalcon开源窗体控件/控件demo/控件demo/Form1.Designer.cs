﻿namespace ViewROI
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_LoadImage = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button22 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.CircleButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.DelActROIButton = new System.Windows.Forms.Button();
            this.Rect2Button = new System.Windows.Forms.Button();
            this.Rect1Button = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button20 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button23 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgv_ROI = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.查看坐标ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.hWindow_Fit1 = new ChoiceTech.Halcon.Control.HWindow_Final();
            this.button21 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ROI)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_LoadImage
            // 
            this.btn_LoadImage.Location = new System.Drawing.Point(20, 4);
            this.btn_LoadImage.Name = "btn_LoadImage";
            this.btn_LoadImage.Size = new System.Drawing.Size(96, 35);
            this.btn_LoadImage.TabIndex = 5;
            this.btn_LoadImage.Text = "打开图片";
            this.btn_LoadImage.UseVisualStyleBackColor = true;
            this.btn_LoadImage.Click += new System.EventHandler(this.btn_LoadImage_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.button22);
            this.groupBox2.Controls.Add(this.button10);
            this.groupBox2.Controls.Add(this.CircleButton);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.DelActROIButton);
            this.groupBox2.Controls.Add(this.Rect2Button);
            this.groupBox2.Controls.Add(this.Rect1Button);
            this.groupBox2.Location = new System.Drawing.Point(171, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(137, 262);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "指定位置绘制";
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(22, 11);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(96, 21);
            this.button22.TabIndex = 12;
            this.button22.Text = "Point";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(22, 188);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(93, 30);
            this.button10.TabIndex = 11;
            this.button10.Text = "CircleArc";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // CircleButton
            // 
            this.CircleButton.Location = new System.Drawing.Point(22, 150);
            this.CircleButton.Name = "CircleButton";
            this.CircleButton.Size = new System.Drawing.Size(96, 34);
            this.CircleButton.TabIndex = 8;
            this.CircleButton.Text = "Circle";
            this.CircleButton.Click += new System.EventHandler(this.CircleButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(22, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 30);
            this.button1.TabIndex = 10;
            this.button1.Text = "Line";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.LineButton_Click);
            // 
            // DelActROIButton
            // 
            this.DelActROIButton.Location = new System.Drawing.Point(22, 221);
            this.DelActROIButton.Name = "DelActROIButton";
            this.DelActROIButton.Size = new System.Drawing.Size(96, 35);
            this.DelActROIButton.TabIndex = 7;
            this.DelActROIButton.Text = "Delete Active ROI";
            this.DelActROIButton.Click += new System.EventHandler(this.DelActROIButton_Click);
            // 
            // Rect2Button
            // 
            this.Rect2Button.Location = new System.Drawing.Point(22, 110);
            this.Rect2Button.Name = "Rect2Button";
            this.Rect2Button.Size = new System.Drawing.Size(96, 35);
            this.Rect2Button.TabIndex = 1;
            this.Rect2Button.Text = "Rectangle2";
            this.Rect2Button.Click += new System.EventHandler(this.Rect2Button_Click);
            // 
            // Rect1Button
            // 
            this.Rect1Button.Location = new System.Drawing.Point(22, 70);
            this.Rect1Button.Name = "Rect1Button";
            this.Rect1Button.Size = new System.Drawing.Size(96, 34);
            this.Rect1Button.TabIndex = 0;
            this.Rect1Button.Text = "Rectangle1";
            this.Rect1Button.Click += new System.EventHandler(this.Rect1Button_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.tabControl1.Location = new System.Drawing.Point(691, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(324, 682);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button21);
            this.tabPage1.Controls.Add(this.button20);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.button12);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.button8);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.btn_LoadImage);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(316, 656);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(20, 369);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(96, 33);
            this.button20.TabIndex = 24;
            this.button20.Text = "显示文字";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button23);
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button16);
            this.groupBox1.Controls.Add(this.button13);
            this.groupBox1.Controls.Add(this.button15);
            this.groupBox1.Controls.Add(this.button14);
            this.groupBox1.Location = new System.Drawing.Point(167, 277);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(141, 128);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "鼠标所在位置绘制";
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(11, 20);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(58, 27);
            this.button23.TabIndex = 22;
            this.button23.Text = "绘制点";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(11, 53);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(58, 27);
            this.button11.TabIndex = 14;
            this.button11.Text = "圆";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(76, 53);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(58, 27);
            this.button16.TabIndex = 19;
            this.button16.Text = "扇形";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(76, 20);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(58, 27);
            this.button13.TabIndex = 16;
            this.button13.Text = "直线";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("宋体", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button15.Location = new System.Drawing.Point(76, 87);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(58, 27);
            this.button15.TabIndex = 18;
            this.button15.Text = "角度矩形";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(11, 87);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(58, 27);
            this.button14.TabIndex = 17;
            this.button14.Text = "矩形";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(20, 329);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(96, 33);
            this.button12.TabIndex = 15;
            this.button12.Text = "刷新";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(20, 289);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(96, 32);
            this.button9.TabIndex = 13;
            this.button9.Text = "禁止编辑";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(20, 250);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(96, 32);
            this.button8.TabIndex = 12;
            this.button8.Text = "允许编辑";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(20, 126);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(96, 27);
            this.button6.TabIndex = 10;
            this.button6.Text = "3显示截图";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(20, 88);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(96, 27);
            this.button5.TabIndex = 10;
            this.button5.Text = "2显示xld";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(20, 50);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(96, 27);
            this.button4.TabIndex = 9;
            this.button4.Text = "1显示region";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(20, 207);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 32);
            this.button3.TabIndex = 8;
            this.button3.Text = "允许缩放";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(20, 164);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 32);
            this.button2.TabIndex = 8;
            this.button2.Text = "禁止缩放";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.dgv_ROI);
            this.groupBox3.Location = new System.Drawing.Point(10, 442);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(300, 206);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "roi列表";
            // 
            // dgv_ROI
            // 
            this.dgv_ROI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ROI.ContextMenuStrip = this.contextMenuStrip1;
            this.dgv_ROI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ROI.Location = new System.Drawing.Point(3, 17);
            this.dgv_ROI.Name = "dgv_ROI";
            this.dgv_ROI.RowTemplate.Height = 23;
            this.dgv_ROI.Size = new System.Drawing.Size(294, 186);
            this.dgv_ROI.TabIndex = 1;
            this.dgv_ROI.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ROI_CellContentClick);
            this.dgv_ROI.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ROI_CellEnter);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看坐标ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            // 
            // 查看坐标ToolStripMenuItem
            // 
            this.查看坐标ToolStripMenuItem.Name = "查看坐标ToolStripMenuItem";
            this.查看坐标ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.查看坐标ToolStripMenuItem.Text = "查看坐标";
            this.查看坐标ToolStripMenuItem.Click += new System.EventHandler(this.查看坐标ToolStripMenuItem_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(544, 542);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(106, 44);
            this.button18.TabIndex = 21;
            this.button18.Text = "加载";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(544, 598);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(106, 44);
            this.button17.TabIndex = 20;
            this.button17.Text = "保存";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(141, 598);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(387, 44);
            this.button7.TabIndex = 11;
            this.button7.Text = "如果第一个元素是矩形，更新图形为指定位置[演示修改已有坐标]";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button25);
            this.groupBox5.Controls.Add(this.button17);
            this.groupBox5.Controls.Add(this.button18);
            this.groupBox5.Controls.Add(this.button24);
            this.groupBox5.Controls.Add(this.button19);
            this.groupBox5.Controls.Add(this.hWindow_Fit1);
            this.groupBox5.Controls.Add(this.button7);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(691, 682);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "groupBox5";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(7, 542);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(128, 44);
            this.button25.TabIndex = 7;
            this.button25.Text = "记录缩放位置";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(7, 598);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(128, 44);
            this.button24.TabIndex = 6;
            this.button24.Text = "恢复为记录位置";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(141, 542);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(387, 44);
            this.button19.TabIndex = 1;
            this.button19.Text = "更新选中方框尺寸为4,4(第一个元素)";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // hWindow_Fit1
            // 
            this.hWindow_Fit1.BackColor = System.Drawing.Color.Transparent;
            this.hWindow_Fit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hWindow_Fit1.DrawModel = false;
            this.hWindow_Fit1.EditModel = true;
            this.hWindow_Fit1.Image = null;
            this.hWindow_Fit1.Location = new System.Drawing.Point(7, 20);
            this.hWindow_Fit1.Margin = new System.Windows.Forms.Padding(4);
            this.hWindow_Fit1.Name = "hWindow_Fit1";
            this.hWindow_Fit1.Size = new System.Drawing.Size(643, 500);
            this.hWindow_Fit1.TabIndex = 0;
            this.hWindow_Fit1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.hWindow_Fit1_MouseClick);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(20, 408);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(96, 33);
            this.button21.TabIndex = 25;
            this.button21.Text = "更改背景";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 682);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ROI)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_LoadImage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button CircleButton;
        public System.Windows.Forms.Button DelActROIButton;
        public System.Windows.Forms.Button Rect2Button;
        public System.Windows.Forms.Button Rect1Button;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgv_ROI;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private ChoiceTech.Halcon.Control.HWindow_Final hWindow_Fit1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 查看坐标ToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
    }
}

