﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ST.Library.UI.NodeEditor;
using 视觉框架测试.STNodeModel;
using 视觉框架测试.ImgSource;
using System.Xml.Linq;
using System.Windows;
using Microsoft.Win32;
using OpenFileDialog = System.Windows.Forms.OpenFileDialog;
using System.IO.Compression;
using System.IO;
using SaveFileDialog = System.Windows.Forms.SaveFileDialog;
using Application = System.Windows.Forms.Application;
using System.Globalization;
using 视觉框架测试.Tool;
using System.Runtime.Serialization.Formatters.Binary;     //序列化与反序列化命名空间
using HalconDotNet;
using System.Runtime.InteropServices.ComTypes;
using System.Diagnostics;
using System.Collections;
using System.Reflection;
using System.Xml;

namespace 视觉框架测试
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();

        }

        private void Form3_Load(object sender, EventArgs e)
        {
            //base.OnLoad(e);
            stNodeEditor1.NodeAdded += (s, ea) => ea.Node.ContextMenuStrip = contextMenuStrip1;


            stNodeTreeView1.LoadAssembly(Application.ExecutablePath);
            stNodeEditor1.LoadAssembly(Application.ExecutablePath);

            /*            stNodeTreeView1.AddNode(typeof(MyNode));
                        stNodeTreeView1.AddNode(typeof(QRCode));

                        stNodeTreeView1.AddNode(typeof(ImageInputNode));
                        stNodeTreeView1.AddNode(typeof(Display));*/


        }
        //保存流程
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var SavePath = Application.StartupPath + @"\配置信息\";
                Stopwatch sp = Stopwatch.StartNew();
                sp.Start();
                if (File.Exists(SavePath + "11.tool"))
                {
                    File.Delete(SavePath + "11.tool");
                }
                if (File.Exists(SavePath + "11.stn"))
                {
                    File.Delete(SavePath + "11.stn");
                }
                if (stNodeEditor1.Nodes.Count != 0)
                {
                    using (FileStream fs = new FileStream(SavePath + "11.tool", FileMode.Create, FileAccess.Write))
                    {
                        STNodeCollection nodes = stNodeEditor1.Nodes;
                        Form1.Guid_tool.Clear();
                        foreach (STNode node in stNodeEditor1.Nodes)
                        {
                            //通过反射调用每个Node的"Get_Tool"方法
                            var node_type = node.GetType();
                            MethodInfo method = node_type.GetMethod("Get_Tool");
                            if (method != null)
                            {
                                var Tool = (BaseTool)method.Invoke(node, null);
                                Form1.Guid_tool.Add(node.Guid, Tool);
                            }
                        }
                        /*                    for (int i = 0; i < nodes.Count; i++)
                                            {
                                                //通过反射调用每个Node的"Get_Tool"方法
                                                var node_type = nodes[i].GetType();
                                                MethodInfo method = node_type.GetMethod("Get_Tool");
                                                if (method != null)
                                                {
                                                    var Tool = (BaseTool)method.Invoke(nodes[i], null);
                                                    Form1.Guid_tool.Add(nodes[i].Guid, Tool);
                                                }
                                            }*/

                        //序列化
                        BinaryFormatter bf = new BinaryFormatter();
                        bf.Serialize(fs, Form1.Guid_tool);

                        fs.Close();
                        //Form1.Guid_tool.Clear();
                    }
                    sp.Stop();
                    var time = sp.ElapsedMilliseconds;
                    stNodeEditor1.SaveCanvas(SavePath + "11.stn");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }





        private void StNodeEditor1_SelectedChanged(object sender, EventArgs e)
        {
            var node = sender as STNodeEditor;
            var noide1 = node.ActiveNode;
        }

        private void stNodeEditor1_DoubleClick(object sender, EventArgs e)
        {
            //var  mouse = e as MouseEventArgs;
            var node = stNodeEditor1.ActiveNode;
            if (node != null)
            {
                //System.Windows.Forms.MessageBox.Show(node.Title);
                if (node.Title == "导入本地图像")
                {
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Title = "请选择文件";
                    dialog.Filter = "所有图像文件 | *.bmp; *.pcx; *.png; *.jpg; *.gif;" + "*.tiff; *.ico;";                //设置文件类型  
                    //设置默认文件类型显示顺序                                                             
                    dialog.FilterIndex = 1;
                    //保存对话框是否记忆上次打开的目录
                    dialog.RestoreDirectory = true;
                    //设置是否允许多选
                    dialog.Multiselect = false;
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        var img_input = node as ImageInputNode;
                        img_input.FileName = dialog.FileName;
                    }

                }
                /*                else if (node.Title == "识别二维码")
                                {
                                    var qr_code = node as STN_QRCode;
                                    qr_code.Show_Set_Form();
                                } else if (node.Title == "形状匹配")
                                {
                                    var ShapeMatching = node as STN_ShapeMatching;
                                    ShapeMatching.Show_Set_Form();
                                }*/
                var node_type = node.GetType();
                MethodInfo Show_Set_Form = node_type.GetMethod("Show_Set_Form");
                if (Show_Set_Form != null)
                {
                    var Tool = (BaseTool)Show_Set_Form.Invoke(node, null);

                }
            }


            //stNodeEditor1.Nodes;
            //System.Windows.Forms.MessageBox.Show("x"+ mouse.X.ToString()+ "\r\n y"+ mouse.Y.ToString());
        }
        private void button2_Click(object sender, EventArgs e)
        {

            var SavePath = Application.StartupPath + @"\配置信息\";
            stNodeEditor1.Nodes.Clear();
            if (File.Exists(SavePath + "11.stn"))
            {
                stNodeEditor1.LoadCanvas(SavePath + "11.stn");
            }
            foreach (STNode node in stNodeEditor1.Nodes)
            {
                //通过反射调用每个Node的"Get_Tool"和“Set_Tool”方法
                //先获取Tool具体类型，再通过“Set_Tool”将反序列化后的对应Tool传进去
                var node_type = node.GetType();
                MethodInfo Get_Tool = node_type.GetMethod("Get_Tool");
                MethodInfo Set_Tool = node_type.GetMethod("Set_Tool");
                if (Get_Tool != null)
                {
                    var Tool = (BaseTool)Get_Tool.Invoke(node, null);
                    Tool = Form1.Guid_tool[node.Guid];
                    object[] obj = new object[1];
                    obj[0] = Tool;
                    if (Set_Tool != null)
                    {
                        //反射调用泛型方法必须加这一句。
                        Set_Tool = Set_Tool.MakeGenericMethod(Tool.GetType());
                        Set_Tool.Invoke(node, obj);
                    }
                }
            }
            button3_Click(null,null) ;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (stNodeEditor1.ActiveNode == null) return;
            stNodeEditor1.Nodes.Remove(stNodeEditor1.ActiveNode);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (stNodeEditor1.ActiveNode == null) return;
            stNodeEditor1.ActiveNode.LockLocation = !stNodeEditor1.ActiveNode.LockLocation;
        }

        private void 解锁节点ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stNodeEditor1.ActiveNode.LockOption = !stNodeEditor1.ActiveNode.LockOption;
        }

        private void stNodeEditor1_NodeAdded(object sender, STNodeEditorEventArgs e)
        {
            var stn_qr = e.Node as STN_QRCode;
            if (stn_qr != null && Form1.Guid_tool != null)
            {
                if (Form1.Guid_tool.ContainsKey(stn_qr.Guid))
                {
                    Type tool_type = Form1.Guid_tool[stn_qr.Guid].Tool_Type;
                    var ss = tool_type.GetType();
                    var tl_q = Convert.ChangeType(Form1.Guid_tool[stn_qr.Guid], tool_type);
                    stn_qr.tool_DataCode = (Tool_DataCode)tl_q;
                }
            }

        }
        public delegate void HandledSetimg(HObject himage);
        public static HandledSetimg handledSetimg;
        private void button3_Click(object sender, EventArgs e)
        {
            STNodeCollection nodes = stNodeEditor1.Nodes;
            foreach (STNode item in nodes)
            {
                if (item.Title == "导入本地图像")
                {
                    ImageInputNode imgnode = item as ImageInputNode;
                    if (imgnode.FileName != null)
                    {
                        HOperatorSet.ReadImage(out HObject Himage, imgnode.FileName);
                        handledSetimg(Himage);
                    }

                }
            }
        }
    }
}
