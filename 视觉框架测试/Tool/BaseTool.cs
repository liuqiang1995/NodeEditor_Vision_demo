﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using ViewWindow.Config;
using ViewWindow.Model;

namespace 视觉框架测试.Tool
{
    [Serializable]
    public class BaseTool
    {
        [NonSerialized]
        private HObject _image;
        private Rectangle2 _search_region;
        [NonSerialized]
        private HObject _symbolXLDs;
        private double _row;
        private double _column;
        private double _angle;
        private double _score;
        private string _result;
        private long _consumeTime;
        private bool _isfist;
        private bool _isOK;
        private double _row_Dvalue;
        private double _column_Dvalue;


        public BaseTool()
        {
            Tool_guid = Guid.NewGuid();
            OutputNode_guid = new List<Guid>();
        }
        /// <summary>
        /// 图片
        /// </summary>
        public HObject Image { 
            set { _image = value; }
            get { return _image; }
        }
        /// <summary>
        /// 搜索区域
        /// </summary>
        public Rectangle2 Search_Region
        { 
            set { _search_region = value; }
            get { return _search_region; }
        }
        /// <summary>
        /// XLD
        /// </summary>
        public HObject SymbolXLDs
        {
            set { _symbolXLDs = value; }
            get { return _symbolXLDs; }
        }
        /// <summary>
        /// 结果行坐标
        /// </summary>
        public double Row { 
            get { return _row; }
            set { _row = value; }
        }
        /// <summary>
        /// 结果列坐标
        /// </summary>
        public double Column { 
            get { return _column; }
            set { _column = value; }
        }
        /// <summary>
        /// 结果角度
        /// </summary>
        public double Angle { 
            set { _angle = value; }
            get { return _angle; }
        }
        /// <summary>
        /// 执行结果分数
        /// </summary>
        public double Score { 
            get { return _score; }
            set { _score = value; } 
            
        }
        /// <summary>
        /// 执行结果
        /// </summary>
        public string Result { 
            get { return _result; }
            set { _result = value; }    
        }
        /// <summary>
        /// 运行消耗时间
        /// </summary>
        public long ConsumeTime
        { 
            get { return _consumeTime; }
            set { _consumeTime = value; }
        }
        /// <summary>
        /// 是否是第一个节点
        /// </summary>
        public bool IsFist
        {
            get { return _isfist; }
            set { _isfist = value; }
        }
        /// <summary>
        /// 结果是否OK
        /// </summary>
        public bool IsOK
        {
            get { return _isOK; }
            set { _isOK = value; }
        }
        /// <summary>
        /// 行坐标差值
        /// </summary>
        public double Row_Dvalue {
            set { _row_Dvalue = value;}
            get { return _row_Dvalue; }
        }
        /// <summary>
        /// 列坐标差值
        /// </summary>
        public double Column_Dvalue { 
            set { _column_Dvalue = value; }
            get { return _column_Dvalue; }
        }


        /// <summary>
        /// STN节点id
        /// </summary>
        public Guid STN_guid { set; get;}



        /// <summary>
        /// 工具id
        /// </summary>
        public Guid Tool_guid { set; get;}
        public Guid InputNode_guid { set; get;}
        public List<Guid> OutputNode_guid { set; get;}
        /// <summary>
        /// 工具类型
        /// </summary>
        public Type Tool_Type { set; get;}
        public virtual void ini()
        { }
        public virtual BaseTool Run(BaseTool baseTool)
        {
            return baseTool;
        }
        public virtual BaseTool Run()
        {
            return new BaseTool();
        }
        public virtual void dispresult()
        { }
        /// <summary>
        /// 基类值传递给派生类
        /// </summary>
        /// <param name="baseTool"></param>
        /// <param name="basetool1"></param>
        /// <returns></returns>
        public object BaseValToDriVal(BaseTool baseTool,BaseTool basetool1)
        {
            Type type = basetool1.GetType();
            //object obj1 = Activator.CreateInstance(type);
            PropertyInfo[] Props = type.GetProperties();
            foreach (PropertyInfo Prop in Props)
            {
                switch (Prop.Name)
                {
                    case "Image":
                        Prop.SetValue(basetool1, baseTool.Image);
                        break;
                    case "Region":
                        Prop.SetValue(basetool1, baseTool.Search_Region);
                        break;
                    case "Row":
                        Prop.SetValue(basetool1, baseTool.Row);
                        break;
                    case "Angle":
                        Prop.SetValue(basetool1, baseTool.Angle);
                        break;
                    case "Column":
                        Prop.SetValue(basetool1, baseTool.Column);
                        break;
                    case "Score":
                        Prop.SetValue(basetool1, baseTool.Score);
                        break;
                    case "Result":
                        Prop.SetValue(basetool1, baseTool.Result);
                        break;
                    default:
                        break;
                }
            }
            return basetool1;

        }
        public static BaseTool DeepCopy(BaseTool baseTool)
        {
            object retval;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, baseTool);

                ms.Seek(0, SeekOrigin.Begin);
                retval = bf.Deserialize(ms);
                ms.Close();
            }
            return (BaseTool)retval;   
        }


    }
}
