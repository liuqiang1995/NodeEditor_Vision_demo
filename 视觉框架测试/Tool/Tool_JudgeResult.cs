﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 视觉框架测试.Tool;

namespace 视觉框架测试.Tool
{
    [Serializable]
    public class Tool_JudgeResult : BaseTool
    {
        public bool Result_isOK { get; set; } = true; 
        public Tool_JudgeResult()
        {
            Tool_Type = typeof(Tool_JudgeResult);
        }


        public override BaseTool Run(BaseTool baseTool)
        {
            BaseTool copy_basetool = DeepCopy(baseTool);
            if (!baseTool.IsOK)
            {
                Result_isOK = false;
            }
            copy_basetool.Image = baseTool.Image;
            copy_basetool.IsOK = Result_isOK;
            copy_basetool.OutputNode_guid = OutputNode_guid;
            copy_basetool.STN_guid = STN_guid;
            return copy_basetool;

        }


    }
}
