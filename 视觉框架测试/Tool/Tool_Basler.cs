﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaslerIPConfig;
using BaslerSDK;
using HalconDotNet;

namespace 视觉框架测试.Tool
{
    public class Tool_Basler : BaseTool
    {
        public string Cam_SN { set; get; }
        public event Action<HObject> CallbackImage;
        public BaslerCam myBasler;
        string[] Cam_SNs;
        string Camera_type = string.Empty;
        public Tool_Basler()
        {
            Tool_Type = typeof(Tool_LocalImg);
            myBaslerIPConfig basler_ipconfig = new myBaslerIPConfig();
            Cam_SNs = basler_ipconfig.get_sn();
            Camera_type = BaslerCam.get_Camera_type();
            if (Cam_SN != null)
            {
                myBasler = new BaslerCam();
                myBasler.OpenCam(Camera_type);
                //myBasler.SetHeartBeatTime(5000);
                //绑定相机事件，获得回传图像
                myBasler.eventProcessImage += processHImage_0;         // 注册halcon显示回调函数
                myBasler.eventComputeGrabTime += computeGrabTime_0;    // 注册计算采集图像时间回调函数

            }
            if (!Is_ini_Cam_state)
            {
                Ini_Cam_state();
            }



        }
        public void Ini_Cam(string SN)
        {
            if (Cam_SN == null && Cam_state.ContainsKey(SN))
            {
                myBasler = new BaslerCam(SN);
                myBasler.OpenCam(Camera_type);
                myBasler.SetFreerun();
                myBasler.KeepShot();
                //myBasler.SetHeartBeatTime(5000);
                //绑定相机事件，获得回传图像
                myBasler.eventProcessImage += processHImage_0;         // 注册halcon显示回调函数
                myBasler.eventComputeGrabTime += computeGrabTime_0;    // 注册计算采集图像时间回调函数

            }
        }
        public void SoftTrigger() 
        {
            myBasler.SetSoftwareTrigger();
            myBasler.SendSoftwareExecute();
        }
        public void ExternTrigger()
        {
            myBasler.SetExternTrigger();
        }

        public static Dictionary<string,bool> Cam_state = new Dictionary<string,bool>();
        public static bool Is_ini_Cam_state = false;
        public void Ini_Cam_state()
        {
            if (Cam_SNs != null)
            {
                if (Cam_SNs.Length > 0)
                {
                    for (int i = 0; i < Cam_SNs.Length; i++)
                    {
                        if (Cam_SNs[i] != null && !Cam_state.ContainsKey(Cam_SNs[i]))
                        {
                            Cam_state.Add(Cam_SNs[i], false);
                        }
                    }
                }
            }

        }



        private void computeGrabTime_0(long time)
        {
            
        }
        HObject Cam_Img = new HObject();
        private void processHImage_0(bool isColor, int width, int height, IntPtr frameAddress)
        {
            try
            {
                HOperatorSet.GenEmptyObj(out Cam_Img);
                // 转换为Halcon黑白图像显示
                HOperatorSet.GenImage1(out Cam_Img, "byte", (HTuple)width, (HTuple)height, (HTuple)frameAddress);
                CallbackImage?.Invoke(Cam_Img);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override BaseTool Run()
        {
            BaseTool baseTool = new BaseTool();
            baseTool.Image = Cam_Img;
            return baseTool;
        }

    }
}
