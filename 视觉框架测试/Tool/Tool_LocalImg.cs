﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 视觉框架测试.Tool
{
    [Serializable]
    public class Tool_LocalImg : BaseTool
    {
        public Tool_LocalImg()
        {
            Tool_Type = typeof(Tool_LocalImg);
/*            Tool_guid = Guid.NewGuid();
            OutputNode_guid = new List<Guid>();*/
        }
        public string Path { set; get; }
        public override BaseTool Run()
        {
            BaseTool baseTool = new BaseTool();
            if (Path != null)
            {
                HObject Himage = new HObject();
                Himage.Dispose();
                HOperatorSet.ReadImage(out Himage, Path);
                Image = Himage;
                baseTool.Image = Himage;
                baseTool.STN_guid = STN_guid;
                baseTool.OutputNode_guid = OutputNode_guid;
            }
            return baseTool;
        }
    }
}
