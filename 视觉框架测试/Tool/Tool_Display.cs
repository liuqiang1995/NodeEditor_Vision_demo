﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewWindow.Config;
using 视觉框架测试.Tool;

namespace 视觉框架测试.Tool
{
    [Serializable]
    public class Tool_Display : BaseTool
    {
        public int HWin_Serial_No { get; set; }
        public Tool_Display()
        {
            Tool_Type = typeof(Tool_Display);
            ini();
        }
        public override void ini()
        {
            Search_Region = new Rectangle2();

        }
        public override BaseTool Run(BaseTool baseTool)
        {
            //记录程序运行时间
            Stopwatch sw = new Stopwatch();
            sw.Start();
            BaseTool DeepCopy_Tool = DeepCopy(baseTool);

            if (baseTool.Search_Region != null)
            {
                if (baseTool.Search_Region.Lenth1 != 0 && baseTool.Search_Region.Lenth2 != 0)
                {
                    HOperatorSet.GetImageSize(baseTool.Image, out HTuple width, out HTuple height);
                    HOperatorSet.SetPart(Form1.setform1.hWindowControl1.HalconWindow, 0, 0, height, width);
                    HOperatorSet.GenRectangle2(out HObject rect2, baseTool.Search_Region.Row, baseTool.Search_Region.Column, baseTool.Search_Region.Phi, baseTool.Search_Region.Lenth1, baseTool.Search_Region.Lenth2);
                    HOperatorSet.SetDraw(Form1.setform1.hWindowControl1.HalconWindow, "margin");
                    if (DeepCopy_Tool.IsOK)
                    {
                        HOperatorSet.SetColor(Form1.setform1.hWindowControl1.HalconWindow, "green");
                    }
                    else
                    {
                        HOperatorSet.SetColor(Form1.setform1.hWindowControl1.HalconWindow, "red");
                    }

                    HOperatorSet.SetLineWidth(Form1.setform1.hWindowControl1.HalconWindow, 2);
                    Form1.setform1.hWindowControl1.HalconWindow.DispObj(baseTool.Image);
                    Form1.setform1.hWindowControl1.HalconWindow.DispObj(rect2);
                }
            }
            sw.Stop();
            DeepCopy_Tool.ConsumeTime = sw.ElapsedMilliseconds;
            DeepCopy_Tool.OutputNode_guid = OutputNode_guid;
            DeepCopy_Tool.STN_guid = STN_guid;
            return DeepCopy_Tool;
        }

    }
}
